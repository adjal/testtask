<?php

use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<div class="site-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute'=>'category_id',
                'label'=>'Категория',
                'format'=>'text',
                'value' => function($model) {
                    return $model->category->name;
                },
                'filter' => $category
            ],
            'price',
        ],
    ]); ?>
</div>
