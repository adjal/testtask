<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $model app\models\Product */
/* @var $category app\models\Category */
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Добавить товар';

echo '<h2>Добавить товар</h2>';

if (!empty(Yii::$app->session->getFlash('success'))) {
    echo '<div class="alert alert-success">' . Yii::$app->session->getFlash('success') . '</div>';
}

$form = ActiveForm::begin();
?>

<?= $form->field($model, 'name') ?>

<?= $form->field($model, 'category_id')->dropDownList($category, ['prompt' => 'Выберите категорию']) ?>

<?= $form->field($model, 'price')->input('number', ['step' => '0.01']) ?>

<?= Html::submitButton('Добавить'   ) ?>

<?php ActiveForm::end() ?>