<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $model app\models\Category */
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Добавить категорию';

echo '<h2>Добавить категорию</h2>';

if (!empty(Yii::$app->session->getFlash('success'))) {
    echo '<div class="alert alert-success">' . Yii::$app->session->getFlash('success') . '</div>';
}

$form = ActiveForm::begin();
?>

    <?= $form->field($model, 'name') ?>

    <?= Html::submitButton('Добавить'   ) ?>

<?php ActiveForm::end() ?>
