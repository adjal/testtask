<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m170929_190356_create_product_table extends Migration
{
    /**
     * Table name
     */
    const TABLE = 'product';

    /**
     * Foreign key name
     */
    const FOREIGN_KEY = 'fk_product-category_id';

    /**
     * Index name
     */
    const INDEX = 'idn-product-category_id';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'category_id' => $this->integer(),
            'price' => $this->decimal('5', '2')
        ]);

        $this->createIndex(
            self::INDEX,
            self::TABLE,
            'category_id'
        );

        $this->addForeignKey(
            self::FOREIGN_KEY,
            self::TABLE,
            'category_id',
            'category',
            'id'
            );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            self::FOREIGN_KEY,
            self::TABLE
        );

        $this->dropIndex(
            self::INDEX,
            self::TABLE
        );

        $this->dropTable(self::TABLE);
    }
}
